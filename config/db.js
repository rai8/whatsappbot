const knex = require('knex')

module.exports = knex({
  client: 'postgres',
  connection: {
    host: process.env.DEV_HOST,
    user: process.env.DEV_USER,
    password: process.env.DEV_PASSWORD,
    database: process.env.DEV_DATABASE,
  },

})