const { Model } = require('objection')
const knex = require('../config/db')

Model.knex(knex)

class Products extends Model {
  //defining table name
  static get tableName() {
    return 'product'
  }

  //defining relationship mapping
  static get relationMappings() {
    return {
      products: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/CommunityLeader.js`,
        join: {
          from: 'products.cleader_id',
          to: 'cleaders.id',
        },
      },
      commission: {
        relation: Model.HasManyRelation,
        modelClass: `${__dirname}/Commission.js`,
        join: {
          from: 'products.comm_id',
          to: 'comm.id',
        },
      },
    }
  }
}

module.exports = Products