const { Model } = require('objection')
const knex = require('../config/db')

Model.knex(knex)

//defining the user model table

class CommunityLeaders extends Model {
  //defining table name
  static get tableName() {
    return 'cleaders'
  }

  //defining relationship mapping
  static get relationMappings() {
    return {
      products: {
        relation: Model.HasOneRelation,
        modelClass: `${__dirname}/Products.js`,
        join: {
          from: 'cleaders.id',
          to: 'product.cleader_id',
        },
      },
      commissions: {
        relation: Model.HasOneThroughRelation,
        modelClass: `${__dirname}/Commission.js`,
        join: {
          from: 'commissions.id',
          through: {
            // persons_movies is the join table.
            from: 'product.comm_id',
            to:   'product.cleader_id'
          },
          to:'cleaders.id'
        }
      }
 
      

    }
  }
}

module.exports = CommunityLeaders

