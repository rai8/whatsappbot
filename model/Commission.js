const { Model } = require('objection')
const knex = require('../config/db')

Model.knex(knex)

//defining the user model table

class Commission extends Model {
  //defining table name
  static get tableName() {
    return 'commissions'
  }

  //defining relationship mapping
  static get relationMappings() {
    return {
      products: {
        relation: Model.HasOneRelation,
        modelClass: `${__dirname}/Products.js`,
        join: {
          from: 'commission.id',
          to: 'product.comm_id',
        },
      },
      

    }
  }
}

module.exports = Commission