const express= require('express')
const { registerUser, getUserInfo, updateCommunityLeader } = require('../controllers/CommunityController')
const router= express.Router()

router.patch('/user/:id', updateCommunityLeader)
router.post('/user', registerUser)
router.get('/users', getUserInfo)
module.exports= router