const express= require('express')
const sendBulk = require('../controllers/bulkSmsController')
const sendMessage = require('../controllers/WhatsappController')
const router= express.Router()


router.get('/send', sendMessage)
router.get('/send-bulk', sendBulk)


module.exports= router

// "whatsapp": {
//     "contentType": "text",
//     "text":"Hello there"
// }}