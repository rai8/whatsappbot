const express= require('express')
const { saveSoldProduct, updateSavedProduct, showTotalCalc } = require('../controllers/ProductController')
const router= express.Router()


router.post('/product', saveSoldProduct)
router.patch('/product/:id', updateSavedProduct)
router.get('/summary', showTotalCalc)


module.exports= router

// items.forEach(x=>{
//     console.log(x)
//     client.messages
//     .create({
//     body: `Hello ${x.fname} ${x.lname}. Your commission for the day is ${x.commission}`,
//     to: +12014743156, // Text this number
//     from: `+${Number(x.phonenumber)}`, // From a valid Twilio number
//     })
//     .then((message) =>{ return res.json(message.sid)});

// })

