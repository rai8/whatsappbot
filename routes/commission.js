const express= require('express')
const { createCommission, updateCommission, getCommission } = require('../controllers/CommissionController')

const router= express.Router()

router.get('/commission', getCommission)
router.post('/commission', createCommission)
router.patch('/commission/:id', updateCommission)
module.exports= router