const CommunityLeaders = require("../model/CommunityLeader")


//registering a new user
const registerUser = async (req, res) => {
  const { fname, lname, email, phonenumber, location} = req.body
  console.log(req.body)
  try {
    const user = {
      fname: fname,
      lname: lname,
      email: email,
      phonenumber:phonenumber,
      location:location
      
    }
    const userExists = await CommunityLeaders.query().findOne({ email })
    if (userExists) {
      return res.json({ status: 'NULL', message: 'Email already exists' })
    }
    const newUser = await CommunityLeaders.query().insert(user)

    res.status(201).json({message:'Community leader saved successfully' })
  } catch (err) {
   return res.status(500).json({
      STATUS: 'NULL',
      Error: err.message,
    })
  }
}

//get users complete info
const getUserInfo = async (req, res) => {
  try {
    const userInfo = await CommunityLeaders.query().withGraphFetched('products').withGraphFetched('commissions',)
    return res.status(201).json(userInfo)
  } catch (err) {
    res.status(500).json({
      STATUS: 'NULL',
      Error: err.message,
    })
  }
}

//update community leader info
const updateCommunityLeader= async(req, res)=>{
  const updateLeader= req.body
  try{
      const updatedProduct= await CommunityLeaders.query().findById(req.params.id).patch(updateLeader)
      res.status(201).json('Record updated successfully')


  }catch(err){
      res.status(500).json({message:err.message})
  }
}

module.exports = { registerUser, getUserInfo, updateCommunityLeader }
