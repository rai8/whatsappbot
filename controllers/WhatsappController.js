const axios= require('axios')
const url = "https://api.tellephant.com/v1/send-message"
const AUTH_TOKEN= process.env.API_TOKEN

//defining headers
let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

const sendMessage=async(req, res)=>{

    try{

        let body = {
            "apikey": AUTH_TOKEN,
            "to": 254799573106,
            "channels": [
                "whatsapp"
            ],
            "whatsapp": {
                "contentType": "template",
                "template": {
                    "templateId": "account_live_new",
                    "language": "en",
                    "components": [
                        {
                            "type" : "body",
                            "parameters" : [
                                {"type" : "text","text" : "Spongeob"},
                                {"type" : "text","text" : "Squarepants" },
                               
                             ]
                        }
                    ]
                }
            }}
      const response=  await axios.post(url,body, {headers})
      res.status(201).json(response.data)

    }catch(err){
       res.status(500).json({message:err.message})
    }

}

module.exports= sendMessage