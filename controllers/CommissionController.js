const Commission = require("../model/Commission")


//create a new commission
const createCommission = async (req, res) => {
  const { range_from, percentage_comm, cleader_id} = req.body
  try {
    const user = {
      cleader_id:cleader_id,
      range_from: range_from,
      percentage_comm: percentage_comm,
      
    }
  
    const newUser = await Commission.query().insert(user)

    res.status(201).json({message:'New commision saved successfully' })
  } catch (err) {
    res.status(500).json({
      STATUS: 'NULL',
      Error: err.message,
    })
  }
}

//update commission

const updateCommission = async (req, res) => {
    const commId = req.params.id
    try {
      const { range_from, percentage_comm, cleader_id } = req.body
      const commission = {
        range_from: range_from,
        percentage_comm: percentage_comm,
        cleader_id:cleader_id
        
      }
      const updatedCommission = await Commission.query().findById(commId).patch(commission)
  
      res.status(201).json({ message: 'Commission updated succssfully' })
    } catch (err) {
      res.status(500).json({
        status: 'NULL',
        message: err.message,
      })
    }
  }

//get total commissions
const getCommission=async (req, res)=>{
  try{
    const commissions= await Commission.query()
    res.status(200).json(commissions)

  }catch(err){
    res.status(200).json({err:err.message})
  }
}
module.exports= {createCommission, updateCommission, getCommission}