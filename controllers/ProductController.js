const Products = require("../model/Products")
const axios= require('axios')


const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

const saveSoldProduct=async(req, res)=>{
    const {product_item, product_qty, product_price, cleader_id, comm_id}= req.body

    try{
       const userSale={ 
           product_item:product_item,
           product_qty: product_qty,
           product_price:product_price,
           cleader_id:cleader_id,
           comm_id:comm_id
        }
        const newSale= await Products.query().insert(userSale)
        res.status(201).json('Record saved successfully')

    }catch(err){
        res.status(500).json({message:err.message})
    }

}

const updateSavedProduct= async(req, res)=>{
    const product= req.body
    try{
        const updatedProduct= await Products.query().findById(req.params.id).patch(product)
        res.status(201).json('Record updated successfully')


    }catch(err){
        res.status(500).json({message:err.message})
    }
}


const showTotalCalc= async(req, res, next)=>{
    const url= 'http://localhost:3005/api/v1/users'
    const response= await axios.get(url)
    const data= response.data
   
    try{
        let items=[]
        data.forEach(x => {
            const totoal_price= parseFloat(x.products.product_price)*parseFloat(x.products.product_qty)
            const commmission= totoal_price* parseFloat(x.commissions.percentage_comm)/100
            var summary={
                id:x.id,
                fname:x.fname,
                lname:x.lname,
                commmission:commmission,
                phonenumber:`+${Number(x.phonenumber)}`,
                location:x.location
            }
            items.push(summary)    
        }
        );
       return res.json(items).status(200)
    
    }catch(err){
        res.json({message:err.message})
    }
   
   
   
   
}

module.exports={saveSoldProduct, updateSavedProduct, showTotalCalc}