const axios=require('axios')
const sendBulk=async (req, res)=>{
   
    const url= 'http://localhost:3005/api/v1/summary'
    const response= await axios.get(url)
    const data= response.data
    
    try{
        data.forEach(async y => {
          
            const {SMS}= AfricasTalking
           try{

               const sendSms= await SMS.send({
                to:y.phonenumber,
                 message: `Hello ${y.fname} ${y.lname}, Your commmission for the day is ${y.commmission}`,
                 enque: true
             })
             return res.status(201).json(sendSms)
           } catch(err){
                res.status(500).json({message:err})
             }
            
        });
           
    }catch(err){
        res.status(500).json({message:err.message})
    }
   
}

module.exports= sendBulk