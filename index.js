require('dotenv').config()
const express =require('express');
const cors =require('cors');
const morgan = require('morgan')

const whatsappRouter= require('./routes/whatsapp')
const productRouter= require('./routes/product.js')
const communityRouter= require('./routes/communities')
const commissionRouter= require('./routes/commission')

const app = express();

const PORT= process.env.PORT||3005
dbUri=process.env.MONGO_URL
//connect to database
require('./config/db')

//middlewares
app.use(morgan('dev'))
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//routes middleware
app.use('/api/v1',whatsappRouter)
app.use('/api/v1',productRouter)
app.use('/api/v1',communityRouter)
app.use('/api/v1',commissionRouter)
app.get('/', (req, res)=>res.status(200).json({message:'Whatsapp bot'}))


// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res.status(err.status || 500).json({
    errors: {
      message: err
    }
  });
});

app.listen(PORT, () => console.log(`App Listening on port ${PORT}`));
