exports.up = async function (knex) {
    if (!(await knex.schema.hasTable('cleaders'))) {
      return await knex.schema.createTable('cleaders', function (table) {
        table.increments('id').primary()
        table.string('fname')
        table.string('lname') 
        table.string('email') 
        table.string('phonenumber') 
        table.string('location') 
        table.timestamp('created_at').defaultTo(knex.fn.now(6))
        table.timestamp('updated_at').defaultTo(knex.fn.now(6))
      })
    }
  }
  
  exports.down = async function (knex, Promise) {
    return await knex.schema.dropTable('cleaders')
  }
