exports.up = async function (knex) {
    if (!(await knex.schema.hasTable('product'))) {
      return await knex.schema.createTable('product', function (table) {
        table.increments('id').primary()
        table.string('product_item')
        table.string('product_qty')
        table.string('product_price') 
        table.integer('cleader_id').notNullable().references('id').inTable('cleaders').onDelete('CASCADE')
        table.integer('comm_id').notNullable().references('id').inTable('commissions').onDelete('CASCADE')
        table.timestamp('created_at').defaultTo(knex.fn.now(6))
        table.timestamp('updated_at').defaultTo(knex.fn.now(6))
      })
    }
  }
  
  exports.down = async function (knex, Promise) {
    return await knex.schema.dropTable('product')
  }
