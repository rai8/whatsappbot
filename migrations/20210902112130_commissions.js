
exports.up = async function (knex) {
    if (!(await knex.schema.hasTable('commissions'))) {
      return await knex.schema.createTable('commissions', function (table) {
        table.increments('id').primary()
        table.string('range_from')
        table.string('percentage_comm') 
        table.integer('cleader_id').notNullable().references('id').inTable('cleaders').onDelete('CASCADE')
        table.timestamp('created_at').defaultTo(knex.fn.now(6))
        table.timestamp('updated_at').defaultTo(knex.fn.now(6))
      })
    }
  }
  
  exports.down = async function (knex, Promise) {
    return await knex.schema.dropTable('commissions')
  }

