module.exports = {
  development: {
    client: 'postgresql',
    connection: 'pg://postgres:Spacenet98@127.0.0.1:5432/whatsapp_bot',
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: './seeds',
    },
  },

};