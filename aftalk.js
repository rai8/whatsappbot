require('dotenv').config()
const credentials = {
    apiKey: process.env.STALKING_TOKEN,       
    username: process.env.STALKING_USERNAME,  
};

const AfricasTalking = require("africastalking")(credentials);
const {SMS}= AfricasTalking
const sendSms=async (num, msg)=>{
    const send= await SMS.send({
        to:num,
         message: msg,
         enque: true
     })
     console.log(send)
}
// sendSms()

module.exports= sendSms